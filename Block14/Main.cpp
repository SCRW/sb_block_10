#include <iostream>
#include <string>
#include <vector>

using std::cout;
using std::endl;

class Animal 
{
public:
	virtual void Voice() {
		cout << "Text" << endl;
	}
};

class Dog : public Animal {
public:
	void Voice() override
	{
		cout << "Woof!" << endl;
	}
};

class Cat : public Animal {
public:
	void Voice() override
	{
		cout << "Meu!" << endl;
	}
};


class Human : public Animal {
public:
	void Voice() override 
	{
		cout << "Kurwa!" << endl;
	}
};

int main()
{	
	Animal* arr[10];
	arr[0] = new Cat();
	arr[1] = new Dog();
	arr[2] = new Cat();
	arr[3] = new Human();
	arr[4] = new Dog();
	arr[5] = new Cat();
	arr[6] = new Human();
	arr[7] = new Dog();
	arr[8] = new Cat();
	arr[9] = new Human();

	for (int i = 0; i < 10; i++) {
		arr[i]->Voice();
	}
	return 0;
}
